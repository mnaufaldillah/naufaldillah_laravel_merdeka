<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Naufaldillah PT 1</title>
    <link rel="stylesheet" href="{{url('css/style.css')}}">
</head>
<body>
    <header>
        <div class="container">
            <div class="header-left">
                <img id="photo" src="https://thecatapi.com/api/images/get?format=src&type=gif" alt="" width="100" height="100">
            </div>

            <div class="header-right">
                <a href="/">CAT</a>
                <a href="/">PARTY</a>
            </div>
        </div>
    </header>

    <div class="top-wrapper">
        <div class="container">
            <h1>WELCOME TO CAT PARTY!</h1>
            <h3>CURRENT TIME</h3>
            <div id="MyClockDisplay" class="clock" onload="showTime()"></div>
            <h3>Cat Party start at 11:55 PM</h3>
        </div>
    </div>

    <footer>
        <div class="container">
            <img id="photo" src="https://thecatapi.com/api/images/get?format=src&type=gif" alt="" width="100" height="100">
            <p>Muhammad Naufaldillah</p>
            <p>Fullstack Engineer</p>
        </div>
    </footer>

    <script src="{{url('js/script.js')}}"></script>
</body>
</html>